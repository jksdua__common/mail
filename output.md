

<!-- Start index.js -->

jshint node:true

## Mail()

Mail client

	@class Mail
	@constructor
	@param {Object} options Mail client options
		@param {Object} options.templates Templates options
			@param {String} options.templates.dir Directory with templates
		@param {Object} options.transport Transport options
			@param {String} options.transport.type Transport type as understood by nodemailer
			@param {Object} options.transport.options Options passed to nodemailer when creating the transport

	@example
		var path = require('path');

		var templatesDir = path.join(__dirname, '/templates');

		var mail = require('js_mail')({
			templates: { dir: templatesDir },
			transport: {
				type: 'Mandrill',
				options: {
					auth: { user: '', pass: '' }
				}
			}
		});

## options

Options used to initialise the instance

	@property options

## transport

Mail transport used to send emails

	@property transport

## template

Loaded email templates

	@property template

## schema

Schema for args passed to various methods

	@property schema
	@type Object

## defaults

Default options for various methods

	@property defaults
	@type Object

## init()

Initialises email
		- loads templates

	@method init
	@async
	
	@example
		yield mail.init();

## send()

Sends an email

	@method send
	@param {Object} options Sending mail options
		@param {Object} options.template Template options
			@param {String} options.template.name Name of the template
			@param {Object} options.template.locals Locals passed to the renderer
		@param {Object} options.mail Mail options. See `nodemailer` for details.
	@async

	@example
		yield mail.send({
			template: {
				name: 'pasta-dinner',
				locals: { firstName: 'John', lastName: 'Smith' }
			},
			mail: {
				from: 'me@tr.ee',
				to: 'me@tr.ee',
				subject: 'Hello world!'
			}
		});

<!-- End index.js -->

