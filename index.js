/* jshint node:true */

'use strict';

var _ = require('lodash');
var assert = require('assert');
var mailer = require('nodemailer');
var jsonschema = require('jsonschema');
var htmlToText = require('nodemailer-html-to-text').htmlToText;
var emailTemplates = require('email-templates');

/*
	Mail client

	@class Mail
	@constructor
	@param {Object} options Mail client options
		@param {Object} options.templates Templates options
			@param {String} options.templates.dir Directory with templates
		@param {Object} options.transport Transport instance
	@throws assert.AssertionError If the options hash is not valid, throws an assertion error

	@example
		var path = require('path');

		var templatesDir = path.join(__dirname, 'templates');

		var mail = require('js_mail')({
			templates: { dir: templatesDir },
			transport: ...
		});
*/
function Mail(options) {
	if (!(this instanceof Mail)) {
		return new Mail(options);
	}

	// valid constructor args
	var res = jsonschema.validate(options, this.schema.constructor);
	assert(res.valid, res.errors);

	this.options = options;
	
	var transport = this.transport = mailer.createTransport(options.transport);
	transport.use('compile', htmlToText(options.htmlToText));
	transport.sendMailThunk = function(options) {
		return function(done) {
			transport.sendMail(options, done);
		};
	};
}

/*
	Options used to initialise the instance

	@property options
*/
Mail.prototype.options = void 0;

/*
	Mail transport used to send emails

	@property transport
*/
Mail.prototype.transport = void 0;

/*
	Loaded email templates

	@property template
*/
Mail.prototype.template = void 0;

/*
	Schema for args passed to various methods

	@property schema
	@type Object
*/
Mail.prototype.schema = {
	constructor: {
		type: 'object',
		required: true,
		properties: {
			templates: {
				type: 'object',
				required: true,
				properties: {
					dir: { type: 'string', required: true }
				}
			},
			transport: {
				type: 'object',
				required: true
			},
			htmlToText: {
				type: 'object',
				required: false
			}
		}
	},
	send: {
		type: 'object',
		required: true,
		properties: {
			template: {
				type: 'object',
				required: true,
				properties: {
					name: { type: 'string', required: true },
					locals: { type: 'object', required: false }
				}
			},
			mail: { type: 'object', required: true }
		}
	}
};

/*
	Default options for various methods

	@property defaults
	@type Object
*/
Mail.prototype.defaults = {
	send: {
		template: {
			locals: {}
		},
		mail: {}
	}
};

/*
	Initialises email
		- loads templates

	@method init
	@async

	@example
		yield mail.init();
*/
Mail.prototype.init = function() {
	var self = this;

	return function(done) {
		var templatesDir = self.options.templates.dir;

		emailTemplates(templatesDir, function(err, template) {
			if (err) { return done(err); }

			self.template = function(name, locals) {
				return function(done) {
					template(name, locals, done);
				};
			};

			done();
		});
	};
};

/*
	Sends an email

	@method send
	@param {Object} options Sending mail options
		@param {Object} options.template Template options
			@param {String} options.template.name Name of the template
			@param {Object} options.template.locals Locals passed to the renderer
		@param {Object} options.mail Mail options. See `nodemailer` for details.
	@throws assert.AssertionError An error is thrown if passed options are not valid
	@async

	@example
		yield mail.send({
			template: {
				name: 'pasta-dinner',
				locals: { firstName: 'John', lastName: 'Smith' }
			},
			mail: {
				from: 'me@tr.ee',
				to: 'me@tr.ee',
				subject: 'Hello world!'
			}
		});
*/
Mail.prototype.send = function *(options) {
	var res = jsonschema.validate(options, this.schema.send);
	assert(res.valid, res.errors);

	// load templates from disk
	if (!this.template) {
		yield this.init();
	}

	var rendered = yield this.template(options.template.name, options.template.locals);
	var mailOptions = _.defaults(options.mail, { html: rendered[0] }, this.defaults.send.mail);

	return yield this.transport.sendMailThunk(mailOptions);
};

module.exports = Mail;
