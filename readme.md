Mail
====

**Email sending helper built on top of nodemailer and email-templates**

It doesn't do anything fancy, it simply makes it quicker to get started. In the future, this will become the place with all the best practices so that doesn't need to be replicated across multiple projects.


Installation
------------

Install the module as a git npm module. For example, to install `v0.2.0` use the following command:

```bash
$ npm install --save git+ssh://git@gitlab.com:jksdua__common/mail.git#v0.2.0
```


Usage
-----

### Step 1 - Create the mail client

```js
var templatesDir = __dirname + '/templates';

var mail = require('js_mail')({
  templates: { dir: templatesDir },
  transport: require('nodemailer-smtp-transport')()
});
```

### Step 2 - Send emails and have fun!

```js
yield mail.send({
  template: {
    name: 'template-a',
    locals: { some: 'param', another: 'param' }
  },
  mail: {
    from: 'test@test.com',
    to: 'test@test.com'
  }
});
```


API Documentation
-----------------

### Mail()

Mail client

  @class Mail
  @constructor
  @param {Object} options Mail client options
    @param {Object} options.templates Templates options
      @param {String} options.templates.dir Directory with templates
    @param {Object} options.transport Transport instance

  @example
    var path = require('path');

    var templatesDir = path.join(__dirname, '/templates');

    var mail = require('js_mail')({
      templates: { dir: templatesDir },
      transport: require('nodemailer-stub-transport')()
    });

### options

Options used to initialise the instance

  @property options

### transport

Mail transport used to send emails

  @property transport

### template

Loaded email templates

  @property template

### schema

Schema for args passed to various methods

  @property schema
  @type Object

### defaults

Default options for various methods

  @property defaults
  @type Object

### init()

Initialises email
    - loads templates

  @method init
  @async

  @example
    yield mail.init();

### send()

Sends an email

  @method send
  @param {Object} options Sending mail options
    @param {Object} options.template Template options
      @param {String} options.template.name Name of the template
      @param {Object} options.template.locals Locals passed to the renderer
    @param {Object} options.mail Mail options. See `nodemailer` for details.
  @async

  @example
    yield mail.send({
      template: {
        name: 'pasta-dinner',
        locals: { firstName: 'John', lastName: 'Smith' }
      },
      mail: {
        from: 'me@tr.ee',
        to: 'me@tr.ee',
        subject: 'Hello world!'
      }
    });

### Generated using the following command:

```
> ./node_modules/.bin/markdox index.js
```


Test
----

Tests can be run using mocha

```bash
$ git clone git@gitlab.com:jksdua__common/mail.git

$ npm install

$ npm test
```


Changelog
---------

### v1.0.0 (19 July 2016)
- Updated dependencies

### v0.2.0 (28 January 2015)
- Bumped dependencies

### v0.1.0 (9 September 2014)
- Updated nodemailer version

### v0.0.1 (7 September 2014)
- Initial commit
