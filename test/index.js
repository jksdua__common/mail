/* jshint node:true */
/* globals describe, it */

'use strict';

describe('#mail', function() {

var co = require('co');
var chai = require('chai');
var expect = chai.expect;

var Mail = require(__dirname + '/..');

var OPTIONS = {
  templates: { dir: __dirname + '/fixtures' },
  transport: require('nodemailer-stub-transport')()
};

describe('#constructor', function() {
  it('should throw an error if passed options are invalid', function() {
    expect(function() {
      new Mail();
    }).to.throw();

    expect(function() {
      new Mail({
        templates: { dir: __dirname + '/templates' },
        transport: function() {}
      });
    }).to.throw();
  });

  it('should work', function() {
    new Mail(OPTIONS);
  });
});

describe('#send', function() {
  var mail = new Mail(OPTIONS);

  it('should throw an error if passed options are invalid', co(function *() {
    var called = false;

    try {
      yield mail.send({
        template: ''
      });
    } catch(e) {
      called = true;
    }

    expect(called).to.equal(true);
  }));

  it('should work with no template locals', co(function *() {
    var messageDetails = yield mail.send({
      template: { name: 'no-locals' },
      mail: { to: 'test@test.com' }
    });
    expect(messageDetails.response.toString()).to.contain('h1');
    expect(messageDetails.envelope.to).to.contain('test@test.com');
  }));

  it('should work with template locals', co(function *() {
    var messageDetails = yield mail.send({
      template: {
        name: 'locals',
        locals: {
          link: { href: 'href', title: 'title' }
        }
      },
      mail: { to: 'test@test.com' }
    });

    expect(messageDetails.response.toString()).to.contain('href');
    expect(messageDetails.response.toString()).to.contain('title');
    expect(messageDetails.envelope.to).to.contain('test@test.com');
  }));
});

});
